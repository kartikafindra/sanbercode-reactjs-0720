//soal nomor 1
var arrayDataPeserta = {
    nama: "Asep",
    jeniskelamin: "Laki-laki",
    hobby: "Membaca buku",
    tahunlahir: 1992
}
console.log(arrayDataPeserta.nama + ' ' + arrayDataPeserta.jeniskelamin
    + ' ' + arrayDataPeserta.hobby + ' ' + arrayDataPeserta.tahunlahir)
//soal nomor 2
var buah = [
    {
        nama: "strawberry",
        warna: "merah",
        ada_bijinya: "tidak",
        harga: "9000 "
    },

    {
        nama: " jeruk",
        warna: " orange",
        ada_bijinya: "ada",
        harga: "8000",
    },
    {
        nama: "semangka",
        warna: "hijau&merah",
        ada_bijinya: "ada",
        harga: "10000",
    },
    {
        nama: "pisang",
        warna: "kuning",
        ada_bijinya: "tidak ada",
        harga: "5000",

    }
]
console.log(buah[0])
//soal no 3
var dataFilm = []
function pushObject(array, data) {
    array.push(data);
};

pushObject(dataFilm, {
    'nama': 'Kimi no Nawa',
    'durasi': '107 menit',
    'genre': 'fantasy, romance',
    'tahun': '2016'
})
console.log(dataFilm);
//soal nomor 4
//release 0
class Animal {
    constructor(parameter1) {
        this._name = parameter1;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }

}
var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//release1

class Ape extends Animal {
    constructor(parameter1) {
        super(parameter1);
        this._legs = 2;
    }
    yell() {
        console.log("Auoooo");
    }
}
class Frog extends Animal {
    constructor(parameter1) {
        super(parameter1);
    }
    jump() {
        console.log("hophop");
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//soal nomor 5
class Clock {
    constructor({ template }) {
        this.template = template;
        this.timer;
    }
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
    stop() {
        clearInterval(timer);
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start(); 