//soal no 1

const luaslingkaran = (r) => {
    const pi = 3.14
    return pi * Math.pow(r, 2);
};
console.log(luaslingkaran(3));

let kelilinglingkaran = (r) => {
    const pi = 3.14
    return 2 * pi * r
};
console.log(kelilinglingkaran(2));

//soal no 2
let kalimat = ""

var semuakata = () => {

    kalimat = "Saya adalah seorang fronted developer"
    return kalimat;
}
console.log(semuakata(`${kalimat}`));

//soal no 3
class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price

    }

}
let allbuku = new Book('buku fisika', 200, 100000)
console.log(allbuku);

class Komik extends Book {
    constructor(name, totalPage, price, iscolorfull) {
        super(name, totalPage, price)
        this.iscolorfull = iscolorfull
    }

}
let allkomik = new Komik('conan', 200, 50000, 'true')
console.log(allkomik);